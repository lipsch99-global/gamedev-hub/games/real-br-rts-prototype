﻿namespace Com.DerDieDas.BRRTS
{
    public static class RoomPropertyKeys
    {
        public const string MapSeed = "mapSeed";
    }

    public static class PlayerPropertyKeys
    {
        public const string SpawnPoint = "spawnPoint";
    }
}