﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Com.DerDieDas.BRRTS
{
    public class PlayerController : MonoBehaviour
    {
        private Player owner;

        public void SetOwner(Player player)
        {
            owner = player;

        }


        [PunRPC]
        private void PlayerInstantiated(PhotonMessageInfo info)
        {
            gameObject.name = info.Sender.NickName;
            transform.SetParent(FindObjectOfType<PlayerManager>().transform);
            SetOwner(info.Sender);
        }
    }
}