﻿using UnityEngine;
using Photon.Pun;
using TMPro;

namespace Com.DerDieDas.BRRTS
{
    public class WaitingManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject forceStartButton;

        [SerializeField]
        private TextMeshProUGUI playerCountText;

        private bool playerCountReached = false;

        // Start is called before the first frame update
        private void Start()
        {
            forceStartButton.SetActive(PhotonNetwork.IsMasterClient);
        }

        // Update is called once per frame
        private void Update()
        {
            if (playerCountReached)
            {
                return;
            }

            if (PhotonNetwork.IsMasterClient && forceStartButton.activeSelf == false)
            {
                forceStartButton.SetActive(PhotonNetwork.IsMasterClient);
            }

            playerCountText.text = PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;

            if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                playerCountReached = true;

                ForceStart();
            }
        }

        public void ForceStart()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.LoadLevel("SpawnPoint");
            }
        }
    }
}