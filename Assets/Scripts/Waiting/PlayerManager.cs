﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Com.DerDieDas.BRRTS
{
    public class PlayerManager : MonoBehaviourPunCallbacks
    {
        public static PlayerController LocalPlayer { get; private set; }

        [SerializeField]
        private GameObject PlayerControllerPrefab;

        [SerializeField]
        private GameObject PlayerCharacterPrefab;


        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            SceneManager.activeSceneChanged += OnSceneChange;
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("OnJoinedRoom");

            InstantiatePlayerController();
        }

        private void InstantiatePlayerController()
        {
            GameObject newPlayerController = PhotonNetwork.Instantiate(PlayerControllerPrefab.name, Vector3.zero, Quaternion.identity);

            PhotonView.Get(newPlayerController).RPC("PlayerInstantiated", RpcTarget.AllBuffered);
        }

        private void OnSceneChange(Scene current, Scene next)
        {
            if (next.name == "Game")
            {
                Debug.Log("OnSceneChange: " + current.buildIndex + " - " + next.name);
                SpawnGameCharacter();
                SceneManager.activeSceneChanged -= OnSceneChange;
            }
        }

        private void SpawnGameCharacter()
        {
            GameManager gm = FindObjectOfType<GameManager>();
            Vector3 spawnPos = gm.GetSpawnPos((string)PhotonNetwork.LocalPlayer.CustomProperties[PlayerPropertyKeys.SpawnPoint]);

            GameObject newPlayerCharacter = PhotonNetwork.Instantiate(PlayerCharacterPrefab.name, spawnPos, Quaternion.identity);

            PhotonView.Get(newPlayerCharacter).RPC("PlayerInstantiated", RpcTarget.AllBuffered);
        }
    }
}