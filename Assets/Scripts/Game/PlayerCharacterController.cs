﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Com.DerDieDas.BRRTS
{
    [RequireComponent(typeof(CameraController), typeof(PlayerArrivalController), typeof(PlayerMovement))]
    public class PlayerCharacterController : MonoBehaviour
    {
        private Player owner;
        private PhotonView photonView;

        private CameraController camController;
        private PlayerArrivalController arrivalController;
        private PlayerMovement movement;

        private CompositeCollider2D waterCollider;

        private bool arrival = true;
        

        private void Start()
        {
            photonView = PhotonView.Get(gameObject);

            camController = GetComponent<CameraController>();
            arrivalController = GetComponent<PlayerArrivalController>();
            movement = GetComponent<PlayerMovement>();

            camController.enabled = photonView.IsMine;
            arrivalController.enabled = photonView.IsMine;
            movement.enabled = false;

        }

        private void Update()
        {
            if (!photonView.IsMine)
            {
                return;
            }
        }

        [PunRPC]
        private void PlayerInstantiated(PhotonMessageInfo info)
        {
            gameObject.name = info.Sender.NickName;
            owner = info.Sender;

        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (arrival)
            {
                arrivalController.enabled = false;
                movement.enabled = true;
                collision.isTrigger = false;
                arrival = false;
            }
        }
    }
}