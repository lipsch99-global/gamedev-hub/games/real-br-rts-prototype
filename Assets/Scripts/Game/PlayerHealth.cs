﻿using Photon.Pun;
using UnityEngine;

namespace Com.DerDieDas.BRRTS
{
    public class PlayerHealth : MonoBehaviour, IPunObservable
    {
        [SerializeField]
        private int maxHealth = 200;

        public int Health { get; private set; }

        // Start is called before the first frame update
        private void Start()
        {
            Health = maxHealth;
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(Health);
            }
            else
            {
                Health = (int)stream.ReceiveNext();
            }
        }

        public void TakeDamage(int amount)
        {
            Health -= amount;
            if (Health < 0)
            {
                Health = 0;
                Die();
            }

        }

        private void Die()
        {
            Debug.Log("Player died");
            gameObject.SetActive(false);
        }

    }
}