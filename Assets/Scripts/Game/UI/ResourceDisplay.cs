﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Com.DerDieDas.BRRTS
{
    public class ResourceDisplay : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI wood;
        [SerializeField]
        private TextMeshProUGUI food;
        [SerializeField]
        private TextMeshProUGUI gold;

        private GameManager gm;

        private void Start()
        {
            gm = FindObjectOfType<GameManager>();
        }

        private void Update()
        {
            wood.text = gm.resources.WoodAmount.ToString();
            food.text = gm.resources.FoodAmount.ToString();
            gold.text = gm.resources.GoldAmount.ToString();
        }
    }
}