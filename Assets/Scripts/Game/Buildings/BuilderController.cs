﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Com.DerDieDas.BRRTS
{
    public class BuilderController : MonoBehaviour
    {
        [SerializeField]
        private BuildingData[] buildings;

        private Tilemap tilemap;
        private MapGenerator mapGenerator;
        private Camera cam;

        private PhotonView photonView;

        private void Start()
        {
            tilemap = FindObjectOfType<Tilemap>();
            mapGenerator = FindObjectOfType<MapGenerator>();
            cam = Camera.main;
            photonView = PhotonView.Get(gameObject);
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 clickPos = cam.ScreenToWorldPoint(Input.mousePosition);
                Vector2Int tilePos = new Vector2Int(Mathf.FloorToInt(clickPos.x + mapGenerator.GetMapSize() / 2), Mathf.FloorToInt(clickPos.y + mapGenerator.GetMapSize() / 2));

                photonView.RPC("PlaceBuilding", RpcTarget.AllBuffered, 0, tilePos.x, tilePos.y);
            }
        }

        [PunRPC]
        private void PlaceBuilding(int buildingIndex, int xPos, int yPos)
        {
            tilemap.SetTile(new Vector3Int(xPos, yPos, 5), BuildingData.GetBuildingDataFromArray(buildings, buildingIndex).TileBase);
        }

    }

    [System.Serializable]
    public struct BuildingWithIndex
    {
        public int index;
        public TileBase building;
    }
}