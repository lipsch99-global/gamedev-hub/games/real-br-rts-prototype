﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Com.DerDieDas.BRRTS
{
    [CreateAssetMenu(fileName = "newBuilding", menuName = "Building/Create new Building")]
    public class BuildingData : ScriptableObject
    {
        public string Name;
        public int Index;
        public TileBase TileBase;
        public ResourcePool BuildRequirements;


        public static BuildingData GetBuildingDataFromArray(BuildingData[] data, int index, int defaultReturn = 0)
        {
            if (data == null ||data.Length < 1)
            {
                return null;
            }
            foreach (var buildingData in data)
            {
                if (buildingData.Index == index)
                {
                    return buildingData;
                }
            }
            if (defaultReturn >= 0)
            {
                return data[defaultReturn];
            }
            return null;
        }
    }
}