﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Com.DerDieDas.BRRTS
{
    [RequireComponent(typeof(CameraController))]
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField]
        private float MoveSpeed = 5;

        private Rigidbody2D rb;

        private PhotonView photonView;

        void Awake()
        {
            photonView = GetComponent<PhotonView>();
            rb = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            if (!photonView.IsMine)
            {
                return;
            }
            Vector2 movement = new Vector2(Input.GetAxis("Horizontal"), 
                Input.GetAxis("Vertical"));
            movement *= MoveSpeed * Time.deltaTime;

            rb.position += movement;
        }
    }
}