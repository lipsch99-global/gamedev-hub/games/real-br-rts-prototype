﻿using Photon.Pun;
using UnityEngine;

namespace Com.DerDieDas.BRRTS
{
    [RequireComponent(typeof(Collider2D))]
    public class Weapon : MonoBehaviour
    {
        [SerializeField]
        private LayerMask playerLayer;

        [SerializeField]
        private int damage;

        private Animator weaponAnimator;

        private PhotonView photonView;

        // Start is called before the first frame update
        private void Start()
        {
            weaponAnimator = GetComponent<Animator>();
            photonView = GetComponent<PhotonView>();
        }

        // Update is called once per frame
        private void Update()
        {
            if (!photonView.IsMine)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                weaponAnimator.SetTrigger("Attack");
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!photonView.IsMine)
            {
                return;
            }
            if (other.transform == transform.parent)
            {
                return;
            }

            PlayerHealth enemy = other.gameObject.GetComponent<PlayerHealth>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }
        }
    }
}