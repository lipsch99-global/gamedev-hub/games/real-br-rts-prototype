﻿using UnityEngine;

namespace Com.DerDieDas.BRRTS
{
    public class PlayerArrivalController : MonoBehaviour
    {
        [SerializeField]
        private float moveSpeed = 5f;

        [SerializeField]
        private float rotationSpeed = 5f;

        private const float movementAngle = 90;
        private float angle = 0;

        private Vector3 initialMoveDirection;
        private Vector3 moveDirection;


        // Start is called before the first frame update
        private void Start()
        {
            initialMoveDirection = (Vector3.zero - transform.position).normalized;
            initialMoveDirection.z = 0;

            moveDirection = initialMoveDirection;
        }

        // Update is called once per frame
        private void Update()
        {
            angle += Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
            angle = Mathf.Clamp(angle, 0 - movementAngle / 2f, movementAngle / 2f);


            moveDirection = Quaternion.AngleAxis(angle, Vector3.back) * initialMoveDirection;

            Debug.DrawRay(transform.position, moveDirection * 20f);
            transform.position += moveDirection * moveSpeed * Time.deltaTime;

        }
    }
}