﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourcePool
{
    public int WoodAmount;
    public int FoodAmount;
    public int GoldAmount;

    #region Operators

    public static ResourcePool operator+(ResourcePool a, ResourcePool b)
    {
        ResourcePool result = new ResourcePool();
        result.WoodAmount = a.WoodAmount + b.WoodAmount;
        result.FoodAmount = a.FoodAmount + b.FoodAmount;
        result.GoldAmount = a.GoldAmount + b.GoldAmount;
        return result;
    }
    public static ResourcePool operator -(ResourcePool a, ResourcePool b)
    {
        ResourcePool result = new ResourcePool();
        result.WoodAmount = a.WoodAmount - b.WoodAmount;
        result.FoodAmount = a.FoodAmount - b.FoodAmount;
        result.GoldAmount = a.GoldAmount - b.GoldAmount;
        return result;
    }

    public static bool operator >(ResourcePool a, ResourcePool b)
    {
        if (a.WoodAmount <= b.WoodAmount)
        {
            return false;
        }
        if (a.FoodAmount <= b.FoodAmount)
        {
            return false;
        }
        if (a.GoldAmount <= b.GoldAmount)
        {
            return false;
        }

        return true;
    }
    public static bool operator <(ResourcePool a, ResourcePool b)
    {
        if (a.WoodAmount >= b.WoodAmount)
        {
            return false;
        }
        if (a.FoodAmount >= b.FoodAmount)
        {
            return false;
        }
        if (a.GoldAmount >= b.GoldAmount)
        {
            return false;
        }

        return true;
    }

    public static bool operator ==(ResourcePool a, ResourcePool b)
    {
        if (a.WoodAmount != b.WoodAmount)
        {
            return false;
        }
        if (a.FoodAmount != b.FoodAmount)
        {
            return false;
        }
        if (a.GoldAmount != b.GoldAmount)
        {
            return false;
        }

        return true;
    }

    public static bool operator !=(ResourcePool a, ResourcePool b)
    {
        return !(a == b);
    }

    public static bool operator >=(ResourcePool a, ResourcePool b)
    {
        if (a.WoodAmount < b.WoodAmount)
        {
            return false;
        }
        if (a.FoodAmount < b.FoodAmount)
        {
            return false;
        }
        if (a.GoldAmount < b.GoldAmount)
        {
            return false;
        }

        return true;
    }

    public static bool operator <=(ResourcePool a, ResourcePool b)
    {
        if (a.WoodAmount > b.WoodAmount)
        {
            return false;
        }
        if (a.FoodAmount > b.FoodAmount)
        {
            return false;
        }
        if (a.GoldAmount > b.GoldAmount)
        {
            return false;
        }

        return true;
    }

    public override bool Equals(object obj)
    {
        var pool = obj as ResourcePool;
        return pool != null &&
               base.Equals(obj) &&
               WoodAmount == pool.WoodAmount &&
               FoodAmount == pool.FoodAmount &&
               GoldAmount == pool.GoldAmount;
    }

    public override int GetHashCode()
    {
        var hashCode = 64182510;
        hashCode = hashCode * -1521134295 + base.GetHashCode();
        hashCode = hashCode * -1521134295 + WoodAmount.GetHashCode();
        hashCode = hashCode * -1521134295 + FoodAmount.GetHashCode();
        hashCode = hashCode * -1521134295 + GoldAmount.GetHashCode();
        return hashCode;
    }

    #endregion
}
