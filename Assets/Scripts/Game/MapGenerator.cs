﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Com.DerDieDas.BRRTS
{
    public class MapGenerator : MonoBehaviourPunCallbacks
    {

        [SerializeField]
        private Tilemap waterTilemap = null;

        [SerializeField]
        private Tilemap terrainTilemap = null;

        [SerializeField]
        private Tilemap objectTilemap = null;

        [SerializeField]
        private TileBase[] tiles = null;

        [Header("Map Generation Details")]
        [SerializeField]
        private float mapDetails = 0.05f;

        [SerializeField]
        private int mapSize = 1;

        [SerializeField]
        private float fallOffBegin = 0.7f;

        [SerializeField]
        private float fallOffLength = 0.2f;

        private int seaSize = 50;

        private float[,] mapValues;

        private bool[,] trees;

        private bool[,] goldOres;

        private int seed = int.MinValue;

        [Header("Trees")]
        [SerializeField]
        private TileBase treeTile;
        [SerializeField]
        private float minTreeHeight;
        [SerializeField]
        private float maxTreeHeight;
        [Range(0.1f, 0.9f)]
        [SerializeField]
        private float treeDetails;
        [Range(0.1f, 0.9f)]
        [SerializeField]
        private float minNoiseValueForTree;
        [Header("Gold")]
        [SerializeField]
        private TileBase goldTile;
        [SerializeField]
        private float mingoldHeight;
        [SerializeField]
        private float maxgoldHeight;
        [Range(0.1f, 0.9f)]
        [SerializeField]
        private float goldDetails;
        [Range(0.1f, 0.9f)]
        [SerializeField]
        private float minNoiseValueForGold;

        // Start is called before the first frame update
        private void Start()
        {
            if (PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(RoomPropertyKeys.MapSeed) == false)
            {
                Debug.LogError("The room had no mapSeed set.");
            }

            seed = (int)PhotonNetwork.CurrentRoom.CustomProperties[RoomPropertyKeys.MapSeed];

            GenerateMap(seed);
            DisplayMap();
        }

        private void GenerateMap(int seed)
        {
            mapValues = new float[mapSize, mapSize];
            trees = new bool[mapSize, mapSize];
            goldOres = new bool[mapSize, mapSize];

            Random.InitState(seed);
            int treeSeed = Random.Range(-5000, 5000);
            int goldSeed = Random.Range(-5000, 5000);

            for (int x = 0; x < mapSize; x++)
            {
                for (int y = 0; y < mapSize; y++)
                {
                    float distanceFromCenter = Vector2Int.Distance(new Vector2Int(x, y), new Vector2Int(mapSize / 2, mapSize / 2));
                    float distanceNormalized = distanceFromCenter.Remap(0, mapSize / 2, 0, 1);
                    distanceNormalized = Mathf.Clamp01(distanceNormalized);

                    float noiseValue = Mathf.PerlinNoise((x + seed) * mapDetails, (y + seed) * mapDetails);

                    if (distanceNormalized >= fallOffBegin + fallOffLength)
                    {
                        mapValues[x, y] = 0;
                    }
                    else if (distanceNormalized >= fallOffBegin)
                    {
                        float factor = (distanceNormalized - fallOffBegin).Remap(0, fallOffLength, 1, 0);
                        mapValues[x, y] = noiseValue * factor;
                    }
                    else
                    {
                        mapValues[x, y] = noiseValue;
                    }

                    trees[x, y] = false;
                    if (mapValues[x, y] >= minTreeHeight && mapValues[x, y] <= maxTreeHeight)
                    {
                        float treeNoise = Mathf.PerlinNoise((x + treeSeed) * treeDetails, (y + treeSeed) * treeDetails);
                        if (treeNoise > minNoiseValueForTree)
                        {
                            trees[x, y] = true;
                        }
                    }

                    goldOres[x, y] = false;
                    if (mapValues[x, y] >= mingoldHeight && mapValues[x, y] <= maxgoldHeight)
                    {
                        float goldNoise = Mathf.PerlinNoise((x + goldSeed) * treeDetails, (y + goldSeed) * goldDetails);
                        if (goldNoise > minNoiseValueForGold)
                        {
                            goldOres[x, y] = true;
                        }
                    }

                }
            }
        }

        private void DisplayMap()
        {
            ClearMap();
            for (int x = -seaSize; x < mapSize + seaSize; x++)
            {
                for (int y = -seaSize; y < mapSize + seaSize; y++)
                {
                    if (x >= 0 && x < mapSize && y >= 0 && y < mapSize)
                    {
                        int tileIndex = Mathf.FloorToInt(Mathf.Clamp(mapValues[x, y].Remap(0, 1, 0, tiles.Length), 0, tiles.Length - 1));
                        if (tileIndex == 0)
                        {
                            waterTilemap.SetTile(new Vector3Int(x, y, 0), tiles[tileIndex]);
                        }
                        else
                        {
                        terrainTilemap.SetTile(new Vector3Int(x, y, 0), tiles[tileIndex]);
                        }
                    }
                    else
                    {
                        waterTilemap.SetTile(new Vector3Int(x, y, 0), tiles[0]);
                    }

                    if (x >= 0 && x < mapSize && y >= 0 && y < mapSize)
                    {
                        if (trees[x, y])
                        {
                            objectTilemap.SetTile(new Vector3Int(x, y, 1), treeTile);
                        }
                        else if (goldOres[x, y])
                        {
                            objectTilemap.SetTile(new Vector3Int(x, y, 1), goldTile);
                        }

                    }
                }
            }
        }

        public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
        {
            if (propertiesThatChanged.ContainsKey("seed") == false)
            {
                return;
            }

            seed = (int)propertiesThatChanged["seed"];
            GenerateMap(seed);
            DisplayMap();
        }

        public int GetMapSize()
        {
            return mapSize;
        }

        private void ClearMap()
        {
            waterTilemap.ClearAllTiles();
            terrainTilemap.ClearAllTiles();
            objectTilemap.ClearAllTiles();
        }

        [ContextMenu("Generate Map")]
        public void EditModeGenerateMap()
        {
            ClearMap();

            seed = Random.Range(-5000, 5000);
            GenerateMap(seed);
            DisplayMap();
        }
    }
}