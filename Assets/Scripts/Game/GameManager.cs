﻿using Photon.Pun;
using UnityEngine;

namespace Com.DerDieDas.BRRTS
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private SpawnPoint[] spawnPoints;

        public ResourcePool resources;

        public Vector3 GetSpawnPos(string pointName)
        {
            foreach (var spawnPoint in spawnPoints)
            {
                if (spawnPoint.Name == pointName)
                {
                    return spawnPoint.Point.position;
                }
            }
            return spawnPoints[Random.Range(0, spawnPoints.Length)].Point.position;
        }
    }

    [System.Serializable]
    internal struct SpawnPoint
    {
        public string Name;
        public Transform Point;
    }
}