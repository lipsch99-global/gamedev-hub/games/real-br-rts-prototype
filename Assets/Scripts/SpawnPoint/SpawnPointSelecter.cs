﻿using UnityEngine;
using Photon.Pun;
using TMPro;

namespace Com.DerDieDas.BRRTS
{
    public class SpawnPointSelecter : MonoBehaviour
    {
        private float startTime;

        [SerializeField]
        private TextMeshProUGUI timeLeft;

        private bool timeElapsed = false;

        private void Start()
        {
            startTime = Time.time;
        }

        private void Update()
        {
            if (timeElapsed)
            {
                return;
            }

            timeLeft.text = (10 - Mathf.RoundToInt(Time.time - startTime)).ToString();

            if (PhotonNetwork.IsMasterClient)
            {
                if ((Time.time- startTime) >= 10)
                {
                    PhotonNetwork.LoadLevel("Game");
                    timeElapsed = true;
                }
            }
        }
        public void SetSpawnPoint(string spawnPoint)
        {
            PhotonNetwork.LocalPlayer.CustomProperties[PlayerPropertyKeys.SpawnPoint] = spawnPoint;
        }
    }
}